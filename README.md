# End-to-End Testing
This document will outline the general components of End-to-End testing from the importance of the tests to how to implement with CI/CD. 
Included in this document will be additional links should further information for a specific question be required. This documnet will serve as a
general outline to End-to-End testing from how to configure up to writing your own tests. There are links included throughout the document that expand upon the information touched upon in the form of tutorials and examples (mainly provided by Gitlab documentation). This information should make researching End-to-End tests and writing End-to-End tests much more simple with helpful resources at hand. 

# Importance of End-to-End Tests:
End-to-End tests are vital to the efficiency and success of a project. They are necessary as they are essentially a precaution. 
End-to-End tests allow developers to run the code through unit tests which in turn, assist in identifying any problems or bugs 
quickly if an issue were to arise. 

# How to Write Tests
Developers can write tests easily with any framework that is supported by WebDriverIO (WebDriverIO allows developers to control a mobile application or a browser easily with minimal lines of code). It is important to note here that while WebDriverIO will be utilized throughout the document, developers may use other software should they choose to do so (Selenium is another popular choice referenced by Gitlab). 

More details can be found here: https://docs.gitlab.com/ee/ci/examples/end_to_end_testing_webdriverio/#writing-tests

# Configure Gitlab CI/CD
There are two main things that need to be accomplished to actually test the code with Gitlab CI/CD...
1. Configure CI/CD Jobs that have a browser available (this should already be set up)
2. Update WebdriverIO configuration to use those browsers to visit review apps

# Beginner's Guide to Writing End-to-End Tests:
Gitlab documentation provides a complete tutorial on how to write efficient and properly configured end-to-end tests. The highlights of the
tutorial include...
1. Determine if an End-to-End test is necessary
2. Understand directory structure within qa/
3. Write a basic End-to-End test 
4. Develop any missing page object libraries 

To read more about writing End-to-End tests, follow the link to the tutorial: https://docs.gitlab.com/ee/development/testing_guide/end_to_end/beginners_guide.html#beginners-guide-to-writing-end-to-end-tests 

More details can be found here: https://docs.gitlab.com/ee/ci/examples/end_to_end_testing_webdriverio/#configuring-gitlab-cicd 




# Additional Resources and Links:
Most of the information contained in this document was found through utilizing the links below.
- https://docs.gitlab.com/ee/ci/examples/end_to_end_testing_webdriverio/
- http://v4.webdriver.io/ 


